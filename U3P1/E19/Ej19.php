<?php
$edad=$_POST['edad'];

switch($edad){
    case "menos de 14 años":
        echo "Eres un bebé";
        break;
    case "de 15 a 20 años":
        echo "Todavía eres muy joven";
        break;
    case "de 21 a 24 años": 
        echo "Eres una persona joven";
        break;
    case "de 41 a 60 años": 
        echo "Eres una persona madura";
        break;
    case "más de 60 años": 
        echo "Ya eres una persona mayor";
        break;
    default:
        echo "Aún no me has dicho tu edad";
        break;
}

?>