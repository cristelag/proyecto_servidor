<?php
# valores iniciales calendario
$month=date("n");
$year=date("Y");
$diaActual=date("j");
 
#  dia de la semana del primer dia
# Devuelve 0 para domingo, 6 para sabado
$diaSemana=date("w",mktime(0,0,0,$month,1,$year))+7;
# ultimo dia del mes
$ultimoDiaMes=date("d",(mktime(0,0,0,$month+1,1,$year)-1));
 
$meses=array(1=>"Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio",
"Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre");
?>
 
<!DOCTYPE html>
<html lang="es">
<head>

	<title>Ejemplo de un simple calendario en PHP</title>
	<meta charset="utf-8">
	<style>

	@import url('https://fonts.googleapis.com/css?family=Amatic+SC&display=swap');
		#calendar {
			
			font-family: 'Amatic SC', cursive;
			font-size:25px;

		}
		#calendar caption {
			text-align:left;
			padding:10px 15px;
			background-color:#484c7f;
			color:#fff;
			font-weight:bold;
		}
		#calendar th {
			background-color:#ac8daf;
			color:#fff;
			width:60px;
		}
		#calendar td {
			text-align:right;
			padding:5px 8px;
			background-color:#ddb6c6;
		}
		
	</style>
</head>
 
<body>

<form action="" method="POST">
<fieldset>
<legend>Vista del calendario</legend>
<strong>Introducir el número del mes</strong>
<input type="text" name="mes"><br/><br/>
<strong>Introducir el año:</strong>
<input type="text" name="anio"><br/><br/>
<input type="submit" name="en" value="Generar calendario">
</fieldset>
</form>

<?php
	if (isset($_POST['en'])){ 
		$mes=$_POST['mes'];
		$anio=$_POST['anio'];
		$date="$anio-$mes-01";
		$dia=date('w',strtotime($date)); //indica a que dia empieza
		$ndias=cal_days_in_month(CAL_GREGORIAN,$mes,$anio);
		$meses=array(1=>"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre");
}
?>
<br/><br/>

<h1>Ejemplo de  calendario en PHP</h1>
<table id="calendar">
	<caption><?php echo "$meses[$mes] $anio"?></caption>
	<tr>
		<th>Lun</th><th>Mar</th><th>Mie</th><th>Jue</th>
		<th>Vie</th><th>Sab</th><th>Dom</th>
	</tr>
	<tr bgcolor="silver">
		<?php
	
		$ultimodia=$dia+$ndias;
		// hacemos un bucle hasta 42, que es el máximo de valores que puede
		// haber:6 columnas de 7 dias
		for($i=1;$i<=42;$i++){
			if($i==$dia){
				$day=1;
			}
			if($i<$dia||$i>=$ultimodia){
				echo "<td>&nbsp;</td>";
			}
			else{
				echo "<td>$day</td>";
				$day++;
			}
			if($i%7==0){
				echo "<tr></tr>\n";
			}
		}
	?>
	</tr>
</table>
</body>
</html>