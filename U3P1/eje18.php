<?php
/*
Hay que usar isset() cuando necesitemos averiguar si una variable está definida en el script que estamos ejecutando, es decir, si existe.
La función empty() hay que usarla cuando queramos averiguar si una variable está vacía o no, es decir, si tiene contenido o no.


*/

//Ejemplo con variable definida
$variable = 'valor';
if (isset($variable)) 
	{
    echo "Variable definida!!!";
	}else
		{
		echo "Variable NO definida!!!";
		}
//ejemplo con empty()
$var= "contenido";
		
if (empty($var)) 
	{
    	echo 'La variable SÍ está vacía, su contenido es: '. $var;
	}
	else{
		echo 'La variable NO está vacía, su contenido es: '. $var;
	}
 
unset($var);
 
if (empty($var)) 
	{
    	echo 'La variable SÍ está vacía, su contenido es: '. $var;
	}
	else{
		echo 'La variable NO está vacía, su contenido es: '. $var;
        }
        


?>