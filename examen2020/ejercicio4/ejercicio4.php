<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>

<body>
    <h1>Películas ordenadas por año</h1>
    <?php
    $conexion = mysqli_connect('localhost', 'root', '', 'video');

    $sql = "SELECT title,year FROM peliculas  ORDER BY year ASC;";
    $resultado = mysqli_query($conexion, $sql);
    if (mysqli_num_rows($resultado) > 0) {
        while ($registro = mysqli_fetch_row($resultado)) {
            echo "<ul>";
            echo "<li>" . $registro[0] . "  " . $registro[1] . "</li>";
            echo "</ul> ";
        }
    }

    ?>

    <?php

    $sqlMax = "SELECT title  FROM peliculas WHERE year=(SELECT max(year) FROM peliculas)";
    $resMax = mysqli_query($conexion, $sqlMax);
    if (mysqli_num_rows($resMax) > 0) {
        while ($reg = mysqli_fetch_row($resMax)) {
            $mayor = $reg[0];

    ?>
            Película más nueva: <?php echo $mayor ?>
    <?php
        }
    }

    ?>

    <?php
    $sqlMin = "SELECT title FROM peliculas WHERE year=(SELECT min(year) FROM peliculas) ";
    $resMin = mysqli_query($conexion, $sqlMin);
    if (mysqli_num_rows($resMin) > 0) {
        while ($reg2 = mysqli_fetch_row($resMin)) {
            $min = $reg2[0];

    ?>
            <br>Película más antigua: <?php echo $min ?>
    <?php
        }
    }

    ?>

</body>

</html>