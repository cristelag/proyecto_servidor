<?php

session_start();

?>
<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Ver películas</title>
</head>

<body>
    <h1>Listado de películas disponibles ordenadas por nota</h1>
    <?php
    $conexion = mysqli_connect('localhost', 'root', '', 'video');
    echo "<table border='1px solid black'>";
    echo "<tr><td>Título</td>";
    echo "<td>Año</td>";
    echo "<td>Director</td>";
    echo "<td>Nota</td></tr>";

    $sql = "SELECT title,year,director,nota FROM peliculas WHERE rented='0' ORDER BY nota DESC;";
    $resultado = mysqli_query($conexion, $sql);

    if (mysqli_num_rows($resultado) > 0) {
        while ($registro = mysqli_fetch_row($resultado)) {
            echo "<tr>";
            echo "<td>" . $registro[0] . "</td>";
            echo "<td>" . $registro[1] . "</td>";
            echo "<td>" . $registro[2] . "</td>";
            echo "<td> " . $registro[3] . "</td>";
        }
    }

    ?>

</body>

</html>