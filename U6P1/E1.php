<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>

<body>
    <?php
    if (!isset($_POST['env'])) {
    ?>
        <form action="" method="POST">
            <input type="text" placeholder="Nombre" name="nom"><br><br>
            <input type="text" placeholder="1 Apellido" name="1ape"><br><br>
            <input type="text" placeholder="2 Apellido" name="2ape"><br><br>
            <input type="number" placeholder="dividendo" name="dividendo"><br><br>
            <input type="number" placeholder="divisor" name="divisor"><br><br>

            <button type="submit" name="env">Calcular</button>

        </form>

    <?php
    } else {
        //error handler function 
        function Error($errno, $errstr)
        {
            echo "<b>Error:</b> [$errno] $errstr <br>";
            echo "Final del script";
            die();
        }

        //set error handler 
        set_error_handler("Error", E_USER_WARNING);
        $divisor = $_POST['divisor'];


        //trigger error 

        if ($divisor == 0) {
            trigger_error("El valor no puede ser 0", E_USER_WARNING);
        }

        $dividendo = $_POST['dividendo'];
        $resul = ($dividendo / $divisor);
        echo "Resultado: $resul";
    }
    ?>


</body>

</html>