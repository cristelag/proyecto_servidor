<!DOCTYPE html>
<html lang="es">
<head>
	<title>Prueba SVG</title>
	<meta charset="utf-8">
</head>
<body>

	<?php

	if (!isset($_POST['enviar'])) {

	?>
	<form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>" method="POST">
		<table style="text-align: center;">

	<?php

		$colors = array("#ff0000","#0066ff","#00ff00");
		for ($i=1; $i<4; $i++) {

	?>		
	
			<tr>
				<td>Porcentaje de votos opción <?php echo $i; ?>:</td>
				<td>
					<input type="number" step="0.01" name="po[]" required="required" max="100">
				</td>
				<td>
					<input type="color" name="co[]" value=<?php echo $colors[$i-1]; ?>>
				</td>
			</tr>

	<?php
	
		}

	?>		

			<tr>
				<td colspan="3">
					<input type="submit" name="enviar" value="Enviar datos y mostrar gráfico">
				</td>
			</tr>
		</table>
	</form>

	<?php

	}
	else {

		if ($_SERVER["REQUEST_METHOD"] == "POST") {

		$total=0;
		foreach ($_POST['po'] as $ind => $votos) {
			$total+=$votos;
		}

		if ($total>100 || $total!=100) {
			echo "Error: el total no puede ser mayor, ni sumar menos del 100%";
		}

		else {
			$conexion=mysqli_connect('localhost', 'votosSVG', 'votosSVG', 'svg');
				if (mysqli_connect_errno()) {
					printf("Conexión fallida %s\n", mysqli_connect_error());
					exit();
				}

				foreach ($_POST['po'] as $ind => $votos) {
					$ind++;
					$sql="UPDATE opciones SET porcentaje=$votos WHERE idop=$ind;";
					$result = mysqli_query ($conexion, $sql);
				}

				$sql="SELECT porcentaje,idop FROM opciones;";
				$result = mysqli_query ($conexion, $sql);
				if(mysqli_num_rows($result) > 0) {
					while ($registro = mysqli_fetch_row($result)) {

			?>
			<svg version="1.1" xmlns="http://www.w3.org/2000/svg" width="1000" height="100">
				<g>
					<rect id=<?php echo "miRect".$registro[1]; ?> style="fill:<?php echo $_POST['co'][$registro[1]-1]; ?>" x="0" y="0" height="100"></rect>
					<text x="10" y="55" fill="black" font-size="20">Opción <?php echo $registro[1].": ".$_POST['po'][$registro[1]-1]; ?>%</text>
				</g>
				<animate xlink:href=<?php echo "#miRect".$registro[1]; ?> attributeName="width" begin="0s" dur="2s" fill="freeze" from="0" to=<?php echo $registro[0]*10; ?> />
				<animate xlink:href=<?php echo "#miRect".$registro[1]; ?> attributeName="fill" from="black" to=<?php echo $_POST['co'][$registro[1]-1]; ?> begin="0s" dur="1s" fill="freeze" />	
			</svg>

			<svg style="padding-left: 25px;" width="100" height="100">
				<circle r="50" cx="50" cy="50" fill="silver" />
				<circle r="25" cx="-30" cy="50" fill="transparent"
		          stroke=<?php echo $_POST['co'][$registro[1]-1]; ?>
		          stroke-width="50"
		          stroke-dasharray="calc(<?php echo $registro[0] ?> * 157/100) 157"
		          transform="rotate(-90) translate(-20)" />
		          <text x="10" y="55" fill="black" font-size="13">Opción <?php echo $registro[1].": ".$_POST['po'][$registro[1]-1]; ?>%</text>
			</svg>

			<?php			

					}
				}

				}
	}

		
	}

	?>
</body>
</html>