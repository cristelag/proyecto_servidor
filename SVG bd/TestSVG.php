<!DOCTYPE html>
<html lang="es">
<head>
	<title>Prueba SVG</title>
	<meta charset="utf-8">
</head>
<body>

	<?php

	if (!isset($_POST['enviar'])) {

	?>
	<form action="" method="POST">
		<table style="text-align: center;">

	<?php

		for ($i=1; $i<4; $i++) {

	?>		
	
			<tr>
				<td>Porcentaje de votos opción <?php echo $i; ?>:</td>
				<td>
					<input type="number" step="0.01" name="po[]" required="required">
				</td>
				<td>
					<input type="color" name="co[]">
				</td>
			</tr>

	<?php
	
		}

	?>		

			<tr>
				<td colspan="3">
					<input type="submit" name="enviar" value="Enviar datos y mostrar gráfico">
				</td>
			</tr>
		</table>
	</form>

	<?php

	}
	else {

		$conexion=mysqli_connect('localhost', 'votosSVG', 'votosSVG', 'svg');
		if (mysqli_connect_errno()) {
			printf("Conexión fallida %s\n", mysqli_connect_error());
			exit();
		}

		foreach ($_POST['po'] as $ind => $votos) {
			$ind++;
			$sql="UPDATE opciones SET porcentaje=$votos WHERE idop=$ind;";
			$result = mysqli_query ($conexion, $sql);
		}

		$sql="SELECT porcentaje,idop FROM opciones;";
		$result = mysqli_query ($conexion, $sql);
		if(mysqli_num_rows($result) > 0) {
			while ($registro = mysqli_fetch_row($result)) {

	?>
	<h2>Opción <?php echo $registro[1]; ?></h2>
	<svg version="1.1" xmlns="http://www.w3.org/2000/svg" width="100" height="100">
		<line stroke=<?php echo $_POST['co'][$registro[1]-1]; ?> stroke-width="50" y1="50" x1="0" y2="50" x2=<?php echo $registro[0]; ?>>
	</svg>
	<p><?php echo $_POST['po'][$registro[1]-1]; ?>%</p>

	<?php			

			}
		}

	}

	?>
</body>
</html>