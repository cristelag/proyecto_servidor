<!DOCTYPE HTML>
<html lang="es">
<head>
	<title>Plantilla básica</title>
	<meta charset='UTF-8'/>
	
</head>
<body> 
<form method="post" action="<?php echo $_SERVER["PHP_SELF"];?>">

<?php
$_SERVER["PHP_SELF"] /* La variable $_SERVER es un tipo de variable especial de php, denominada «supervariable». Es capaz de comportarse como una variable global, sin necesidad de declararla como tal. Es creada por el servidor web. En ella almacena información, creando un array. Con php_self obtenemos el nombre del script que esta ejecutandose ahora. */

//lo que haría ese formulario sería redirigir la página una vez enviado el formulario a la página actual, sin necesidad de saber la ruta
?>
</body>
</html>