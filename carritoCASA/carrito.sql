-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 27-01-2020 a las 14:17:30
-- Versión del servidor: 10.1.36-MariaDB
-- Versión de PHP: 7.2.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `carrito`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `articulo`
--

CREATE TABLE `articulo` (
  `idproducto` int(11) NOT NULL,
  `nombreproducto` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `descripcion` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `precio` int(5) NOT NULL,
  `rutaimagen` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `articulo`
--

INSERT INTO `articulo` (`idproducto`, `nombreproducto`, `descripcion`, `precio`, `rutaimagen`) VALUES
(3, 'Lord Dominiks ', '+45 daño', 2800, 'imagenes/dominik.png'),
(4, 'Darkseal', '+10 de poder ', 350, 'imagenes/darkseal.png'),
(5, 'Phantom', '\r\n+25% de crítico\r\n', 2600, 'imagenes/phantom.png'),
(6, 'Titanic Hydra', '+40 de daño\r\n\r\n', 3500, 'imagenes/titanic.png'),
(7, 'Rylai Crystal ', '\r\n+300 de vida', 2600, 'imagenes/rylai.png'),
(8, 'Rapid Fire', '+30% de velocidad \r\n', 2600, 'imagenes/rapidfire.png'),
(9, 'Stopwatch', 'otorga inmunidad', 600, 'imagenes/stopwatch.png'),
(10, 'Ward', 'otorga visión', 5, 'imagenes/ward.jpg');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `compras`
--

CREATE TABLE `compras` (
  `idcompra` int(11) NOT NULL,
  `idarticulo` varchar(4) COLLATE utf8_unicode_ci NOT NULL,
  `idcliente` int(11) NOT NULL,
  `fecha` datetime NOT NULL,
  `cantidad` int(3) NOT NULL,
  `precio` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `compras`
--

INSERT INTO `compras` (`idcompra`, `idarticulo`, `idcliente`, `fecha`, `cantidad`, `precio`) VALUES
(9, '5', 5, '2020-01-25 18:57:40', 1, 2600),
(20, '4', 7, '2020-01-25 20:46:36', 2, 350),
(26, '3', 5, '2020-01-26 16:25:15', 1, 2800),
(50, '6', 5, '2020-01-27 09:31:55', 1, 3500),
(51, '6', 5, '2020-01-27 09:33:01', 1, 3500),
(52, '6', 5, '2020-01-27 09:33:12', 1, 3500),
(67, '5', 5, '2020-01-27 09:43:20', 2, 2600),
(68, '3', 5, '2020-01-27 09:43:39', 1, 2800),
(69, '6', 5, '2020-01-27 09:43:39', 1, 3500),
(70, '4', 5, '2020-01-27 09:43:39', 1, 350),
(71, '5', 5, '2020-01-27 09:43:39', 2, 2600),
(72, '3', 5, '2020-01-27 09:44:02', 1, 2800),
(73, '6', 5, '2020-01-27 09:44:02', 1, 3500),
(74, '4', 5, '2020-01-27 09:44:02', 1, 350),
(75, '5', 5, '2020-01-27 09:44:02', 2, 2600),
(76, '3', 5, '2020-01-27 09:44:03', 1, 2800),
(77, '6', 5, '2020-01-27 09:44:03', 1, 3500),
(78, '4', 5, '2020-01-27 09:44:03', 1, 350),
(79, '5', 5, '2020-01-27 09:44:03', 2, 2600),
(80, '3', 5, '2020-01-27 09:44:03', 1, 2800),
(81, '6', 5, '2020-01-27 09:44:03', 1, 3500),
(82, '4', 5, '2020-01-27 09:44:03', 1, 350),
(83, '5', 5, '2020-01-27 09:44:03', 2, 2600),
(84, '3', 5, '2020-01-27 09:44:03', 1, 2800),
(85, '6', 5, '2020-01-27 09:44:03', 1, 3500);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuarios`
--

CREATE TABLE `usuarios` (
  `idusuario` int(11) NOT NULL,
  `usuario` varchar(20) NOT NULL,
  `password` longtext NOT NULL,
  `rol` enum('gestor','cliente') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `usuarios`
--

INSERT INTO `usuarios` (`idusuario`, `usuario`, `password`, `rol`) VALUES
(1, 'mac', '2d0196a5b689802f4d35c3648cd95a377c5d34bd61a92cbefd4fa24eaef3a95405757de6883cc82a72501e41591d455b82da26c57011568475b419208063894f', 'gestor'),
(4, 'bea', '2d0196a5b689802f4d35c3648cd95a377c5d34bd61a92cbefd4fa24eaef3a95405757de6883cc82a72501e41591d455b82da26c57011568475b419208063894f', 'cliente'),
(5, 'cristelag', '2d0196a5b689802f4d35c3648cd95a377c5d34bd61a92cbefd4fa24eaef3a95405757de6883cc82a72501e41591d455b82da26c57011568475b419208063894f', 'cliente'),
(6, 'carlos', '2d0196a5b689802f4d35c3648cd95a377c5d34bd61a92cbefd4fa24eaef3a95405757de6883cc82a72501e41591d455b82da26c57011568475b419208063894f', 'cliente'),
(7, 'croqueta', '2d0196a5b689802f4d35c3648cd95a377c5d34bd61a92cbefd4fa24eaef3a95405757de6883cc82a72501e41591d455b82da26c57011568475b419208063894f', 'cliente'),
(12, 'isa', '2d0196a5b689802f4d35c3648cd95a377c5d34bd61a92cbefd4fa24eaef3a95405757de6883cc82a72501e41591d455b82da26c57011568475b419208063894f', 'cliente'),
(13, 'emmanuel', '2d0196a5b689802f4d35c3648cd95a377c5d34bd61a92cbefd4fa24eaef3a95405757de6883cc82a72501e41591d455b82da26c57011568475b419208063894f', 'cliente'),
(15, 'rogelio', '2d0196a5b689802f4d35c3648cd95a377c5d34bd61a92cbefd4fa24eaef3a95405757de6883cc82a72501e41591d455b82da26c57011568475b419208063894f', 'cliente');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `articulo`
--
ALTER TABLE `articulo`
  ADD PRIMARY KEY (`idproducto`);

--
-- Indices de la tabla `compras`
--
ALTER TABLE `compras`
  ADD PRIMARY KEY (`idcompra`),
  ADD KEY `idcliente` (`idcliente`),
  ADD KEY `idarticulo` (`idarticulo`) USING BTREE;

--
-- Indices de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  ADD PRIMARY KEY (`idusuario`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `articulo`
--
ALTER TABLE `articulo`
  MODIFY `idproducto` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT de la tabla `compras`
--
ALTER TABLE `compras`
  MODIFY `idcompra` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=271;

--
-- AUTO_INCREMENT de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  MODIFY `idusuario` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `compras`
--
ALTER TABLE `compras`
  ADD CONSTRAINT `compras_ibfk_1` FOREIGN KEY (`idcliente`) REFERENCES `usuarios` (`idusuario`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
