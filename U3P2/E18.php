<!DOCTYPE HTML>
<html lang="es">
<head>
	<title>Plantilla básica</title>
	<meta charset='UTF-8'/>
	<style>
    table{
        border: 1px solid black;
        border-collapse: collapse;
    }
    td{
        border: 1px solid black;
  
    }
    </style>
</head>
<body> 
    <?php
$lenguaje_servidor=array(
    "a1"=>'Arrays',
    "a2"=>'Bases de datos',
    "a3"=>'Formularios');
$lenguaje_cliente=array(
    "b1"=>'Funciones',
    "b2"=>'JavaScript',
    "b3"=>'Bucles');
$resultado=array_merge($lenguaje_cliente,$lenguaje_servidor);



?>
<table>
<legend><strong>Datos de cliente y servidor</strong></legend>
<?php
foreach ($resultado as $key => $value) {
echo '<tr><td>'.$value.'</td></tr>';
}
?>
</table>
</body>
</html>
