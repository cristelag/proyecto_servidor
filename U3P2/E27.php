<!DOCTYPE HTML>
<html lang="es">
<head>
	<title>Plantilla básica</title>
	<meta charset='UTF-8'/>
	<style>
   
    </style>
</head>
<body> 

<?php
$deportes=array("Fútbol","Baloncesto","Natación","Tenis");
for ($i=0; $i <count($deportes) ; $i++) { 
    echo "$deportes[$i]<br/>";
}
echo '<br/>Total de valores del array:'.count($deportes).'<br/>';

echo 'El puntero se ha situado en: '.current($deportes).'<br/>';
next($deportes);
echo 'Posición actual del puntero: '.current($deportes).'<br/>';
end($deportes);
echo 'El puntero se sitúa en la última posición: '.current($deportes).'<br/>';
prev($deportes);
echo 'El puntero ha retrocedido una posición: '.current($deportes).'<br/>';

?>


</body>
</html>