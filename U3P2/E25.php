<!DOCTYPE HTML>
<html lang="es">
<head>
	<title>Plantilla básica</title>
	<meta charset='UTF-8'/>
	<style>
    table{
        border: 1px solid black;
        border-collapse: collapse;
    }
    td{
        border: 1px solid black;
  
    }
    </style>
</head>
<body> 

<?php
$numeros=array("Barcelona"=>"Camp Nou", "Real Madrid"=>"Santiago Bernabeu", "Valencia"=>"Mestalla","Real Sociedad"=>"Anoeta");

?>
<table>

<?php

$numeros=array(
    "5"=>1,
    "12"=>2,
    "13"=>56,
    "x"=>42,
);
foreach ( $numeros as $indice=>$nombre ) {
    echo "<tr><td>$indice </td><td>$nombre </td></tr>";
    }
echo 'Hay un total de '.count($numeros).' elementos en el array';

?>

</table>
<table>
<?php
echo '<br/>;'
unset($numeros["5"]);
foreach ( $numeros as $indice=>$nombre ) {
    echo "<tr><td>$indice </td><td>$nombre </td></tr>";
    }

?>
</table>;


</body>
</html>