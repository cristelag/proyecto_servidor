<!DOCTYPE html>
<html>
<head>
	<title>Ejercicio 2</title>
  <link href="https://fonts.googleapis.com/css?family=Emilys+Candy&display=swap" rel="stylesheet">
  <link href="https://fonts.googleapis.com/css?family=Mallanna&display=swap" rel="stylesheet">
  <style>
  img{
    width:auto;
    height:100px;
    
  }
  fieldset{
    width:220px;
    height:300px;
    border:7px dashed #faf5ef;
  }
  table {
    float:left;
  }
  #carrito{
    float:right;
    border: 3px dashed #faf5ef;
    padding: 5px;
    background-color:rgba(98,84,73,.3);
  }
  body{
    background-image:url("imagenes/fondo2.jpg");
    background-size: contain;
    font-family: 'Mallanna', sans-serif;
    padding:15px;
  }

  h1{
    font-family: 'Emilys Candy', cursive;
    color:#672f2f
    

  }
  </style>
</head>
<body>
	<?php 
  $paso=0;
	session_start();
  if (isset($_POST["ingresar"])) {
    $user = $_POST["nombre"];
    $_SESSION["nombre"]=$user;
    //creamos un array con los productos 
    $carrito = array('jersey1' => 0,'vestido1' => 0,'abrigo' => 0,'jersey2' => 0,'traje1' => 0,'vestido2' => 0 );
    $precio = array('jersey1' => 210,'vestido1' => 300,'abrigo' => 13,'jersey2' => 20,'traje1' => 30,'vestido2' => 5 );
  
//creamos una sesión con el array de productos y precios respectivamente
    $_SESSION["productos"] = $carrito;
    $_SESSION["precios"] = $precio;
  }
  echo ("<center><h1>¡Bienvenid@, ".$_SESSION["nombre"]."!</h1></center>");

  //le damos a cada elemento del array, un propio array con sus propiedades
  $jersey1 = array("Producto" => "Jersey Rayas","Descripcion" => "jersey de rayas.","Precio" => 210,"imagen"=>"imagenes/jersey.jpg");
  
  $vestido1 = array("Producto" => "Vestido dulce","Descripcion" => "Vestido dulce","precio" => 300, "imagen"=>"imagenes/vestido1.jpg");

  $abrigo = array("Producto" => "Abrigo de invierno","Descripcion" => "Abrigo doble faz","precio" => 13, "imagen"=>"imagenes/abrigo1.jpg");
  
  $jersey2 = array("Producto" => "Sudadera rosa","Descripcion" => "Sweter calentito","precio" => 20,"imagen" =>"imagenes/jersey2.jpg");
  
  $traje1 = array("Producto" => "Americana larga","Descripcion" => "Americana larga","precio" => 30,"imagen"=>"imagenes/traje1.jpg");
  
	$vestido2 = array("Producto" => "Vestido verde","Descripcion" => "Vestido verde","precio" => 5,"imagen"=>"imagenes/vestido2.jpg");
 
	?>
	<br>
	<br>
	<?php
  $lista = array("jersey1"=>$jersey1,"vestido1"=>$vestido1,"abrigo"=>$abrigo, "jersey2"=>$jersey2,"traje1"=>$traje1,"vestido2"=>$vestido2);
  $_SESSION["lista"]=$lista;
  ?>

  <form action="control.php" method="POST"> 
    <table>
      <tr>
        <?php
        $i = 1;
        //pintamos los productos en una tabla
        foreach ($lista as $key => $value) {
          echo "<td><fieldset>";
          foreach ($value as $campo => $dato) { ?>
            <p><?php if($campo=="imagen"){
              echo "<center><img src='$dato'></img></center>";
            }else{
              echo $campo ?>: <b><?php echo $dato;}}
              ?>
              </b></p>
              
            <button type="submit" class="ag" name="agregar" value=<?php echo $key;?>>agregar</button>

            </fieldset>
            </td>
          <?php
            $i++;
            if ($i % 4 == 0) {
              echo "</tr><tr>";
            }
          }
          ?>
      </tr>
    </table>
  </form>
  <br><br><br>
  <div id="carrito">
  <h3>CARRITO COMPRA</h3>
  <ul>
    <?php
    echo "<form action='control.php' method=POST>";
    foreach ($_SESSION["productos"] as $key => $prod) {
      if ($prod>0) {
        $precio=($prod*$_SESSION["precios"][$key]);
     echo "<li>Producto:".$lista[$key]['Producto']." x $prod Precio: ".$precio."€";
      echo "<button type='submit' name='quitar' value='$key'>Quitar </li><br>";
      }
    }

    echo "</form>";

    ?>
  </ul>
  <form action="confirmar.php" method="POST">
  	<button type="submit" name="confirmar" value="Confirmar compra">Confirmar compra</button>
  </form>
  </div>
</body>
</html>