<?php

session_start();

?>

<!DOCTYPE html>
<html lang="es">
<head>
	<title>Alta médico</title>
	<meta charset="utf-8">
	<link rel="stylesheet" type="text/css" href="estilogeneral.css">
	<link href="https://fonts.googleapis.com/css?family=Marcellus+SC&display=swap" rel="stylesheet">

<style>
.formu{
	background-color:white;
	width:500px;
	height:500px;
	padding:30px;
	margin-left:auto;
	margin-right:auto;
	font-size:16px;
	box-shadow: 6px 6px 6px black;


}
input{
	border-radius:5%;
	border: 1px solid grey;
	padding:5px;
	font-family:'Open Sans', sans-serif;

}
select{
	border-radius:5%;
	border: 1px solid grey;
	padding:5px;
	font-family:'Open Sans', sans-serif;

}
legend{
	text-align:center;
}
.asig{
	margin-top:15px;
    background: #0f4c75;
    width: 125px;
    padding-top: 5px;
    padding-bottom: 5px;
    color: white;
    border-radius: 4px;
	border: #3282b8 1px solid;
	cursor:pointer;
	
}
.asig:hover{
	background: #3282b8;
	
}
h1,h2{
	color:white;
	font-family: 'Marcellus SC', serif;

}
</style>
</head>
<body>
	<h1>Bienvenido/a <?php echo $_SESSION['usuario']; ?>, se ha identificado como <?php echo $_SESSION['usutipo'] ?></h1>
	<div>
		<form action="" method="POST">
			<button type="submit" name="back">Volver al menú</button>
			<button type="submit" name="logout">Cerrar Sesión</button>
		</form>
	</div>
	<form action="" method="POST" name="altaForm" onsubmit="return validar()" class="formu">
	
			<legend><strong>Dar de alta a un médico</strong></legend>
			<p>DNI: <input type="text" name="dni" id="nif" required="required" onblur="valdni()" maxlength="10"><span id="avisodni"></span></p>
			<p>Nombre: <input type="text" name="nombre" required="required" maxlength="50"></p>
			<p>Apellidos: <input type="text" name="apell" required="required" maxlength="50"></p>
			<p>Especialidad: <input type="text" name="especi" required="required" maxlength="50"></p>
			<p>Teléfono: <input type="text" name="tlf" id="tlf" required="required" maxlength="15" onblur="valtlf()"><span id="avisotlf"></span></p>
			<p>Email: <input type="text" name="mail" id="mail" required="required" maxlength="50" onblur="valmail()"><span id="avisomail"></span></p>
			<p>Usuario: <input type="text" name="usu" required="required" maxlength="15"></p>
			<p>Contraseña: <input type="password" name="pass" required="required" maxlength="157"></p>
			<p>Repita la contraseña: <input type="password" name="rpass" id="rpasswd" required="required" maxlength="157" onblur="valpass()"><span id="avisopass"></span></p>
			<p>Estado: <select name="estado" required="required">
				<option value="Activo">Activo</option>
				<option value="Inactivo">Inactivo</option>
			</select></p>
			<p><input type="submit" class="asig" name="insertar" value="Dar de alta"></p>
	
	</form>

	<?php

	if (isset($_POST['insertar'])) {

		$dni=$_POST['dni'];
		$nom=$_POST['nombre'];
		$apell=$_POST['apell'];
		$special=$_POST['especi'];
		$tlf=$_POST['tlf'];
		$mail=$_POST['mail'];
		$usu=$_POST['usu'];
		$pass=$_POST['pass'];

		if ($_SESSION['usutipo']=='Administrador') {

			$conexion=mysqli_connect('localhost', 'administrador','', 'consultas');
			if (mysqli_connect_errno()) {
	    		printf("Conexión fallida %s\n", mysqli_connect_error());
	    		exit();
			}

			$sql="INSERT INTO medicos (dniMed,medNombres,medApellidos,medEspecialidad,medTelefono,medCorreo) VALUES ('$dni','$nom','$apell','$special','$tlf','$mail');";
			$sql2="INSERT INTO usuarios (dniUsu,usuLogin,usuPassword,usuEstado,usutipo) VALUES ('$dni','$usu','$pass','Activo','Medico');";
			if (mysqli_query($conexion, $sql) && mysqli_query($conexion, $sql2)) {
			 	echo "<p> Se ha registrado el médico con éxito</p>";
			}
			else {
				echo " <br> Error: " . $sql . "<br>" . mysqli_error($conexion);
				echo " <br> Error: " . $sql2 . "<br>" . mysqli_error($conexion);
			}

		}
		mysqli_close($conexion);
	}

	if (isset($_POST['back'])) {

		header("Location:inicio.php");

	}

	if (isset($_POST['logout'])) {

		session_destroy();
			 
		header("Location:acceso.php");
	}

	?>
	<script>
		
		function validar() {
			if (valdni() && valtlf() && valmail() && valpass()) {
				return true;
			}
			else {
				alert ("Datos erróneos, indtroducir de nuevo");
				return false;
			}
		}

		function valdni() {
			var nif = document.altaForm.dni.value;
			var expresion_regular_dni
		 
		  	expresion_regular_dni = /^\d{8}[a-zA-Z]$/;
		 
		  	if (expresion_regular_dni.test (nif) == true) {
		  		document.getElementById('nif').style.border="3px solid green";
		    	document.getElementById('avisodni').innerHTML=" &check; DNI correcto";
		    	return true;
		  	}
		  	else {
		  		document.getElementById('nif').style.border="3px solid red";
		  		document.getElementById('avisodni').innerHTML=" &cross; DNI erróneo, formato no válido";
		  		return false;
		   	}	
		}

		function valtlf() {
			var regexptlf = /^[56789]\d{8}/
			var tlf = document.altaForm.tlf.value;
			if (tlf.match(regexptlf)) {
				document.getElementById('tlf').style.border="3px solid green";
		    	document.getElementById('avisotlf').innerHTML=" &check; Teléfono correcto";
				return true;
			}
			else {
				document.getElementById('tlf').style.border="3px solid red";
		  		document.getElementById('avisotlf').innerHTML=" &cross; Teléfono no válido";
				return false;
			}
		}

		function valmail() {
			var regexpmail = /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/
			var mail = document.altaForm.mail.value;
			if (mail.match(regexpmail)) {
				document.getElementById('mail').style.border="3px solid green";
		    	document.getElementById('avisomail').innerHTML=" &check; Email correcto";
				return true;
			}
			else {
				document.getElementById('mail').style.border="3px solid red";
		    	document.getElementById('avisomail').innerHTML=" &cross; Email incorrecto";
				return false;
			}
		}

		function valpass() {
			var contra=document.altaForm.pass.value;
			var rcontra=document.altaForm.rpass.value;

			if (contra===rcontra) {
				document.getElementById('rpasswd').style.border="3px solid green";
				document.getElementById('avisopass').innerHTML=" &check; Contraseña correcta";
				return true;
			}
			else {
				document.getElementById('rpasswd').style.border="3px solid red";
				document.getElementById('avisopass').innerHTML=" &cross; Contraseña incorrecta";
				return false;
			}
		}

	</script>
</body>
</html>