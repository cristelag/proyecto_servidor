# Privilegios para `acceso`@`%`

GRANT USAGE ON *.* TO 'acceso'@'%';

GRANT SELECT (usuPassword, usutipo, dniUsu, usuLogin) ON `consultas`.`usuarios` TO 'acceso'@'%';


# Privilegios para `administrador`@`%`

GRANT USAGE ON *.* TO 'administrador'@'%';

GRANT ALL PRIVILEGES ON `consultas`.* TO 'administrador'@'%' WITH GRANT OPTION;


# Privilegios para `asistente`@`%`

GRANT USAGE ON *.* TO 'asistente'@'%';

GRANT SELECT ON `consultas`.`medicos` TO 'asistente'@'%';

GRANT SELECT ON `consultas`.`consultorios` TO 'asistente'@'%';

GRANT SELECT, INSERT (citMedico, idCita, citFecha, citHora, citEstado, citPaciente, citConsultorio) ON `consultas`.`citas` TO 'asistente'@'%';

GRANT SELECT (dniUsu), INSERT ON `consultas`.`usuarios` TO 'asistente'@'%';

GRANT SELECT, INSERT ON `consultas`.`pacientes` TO 'asistente'@'%';


# Privilegios para `medico`@`%`

GRANT USAGE ON *.* TO 'medico'@'%';

GRANT SELECT ON `consultas`.`consultorios` TO 'medico'@'%';

GRANT SELECT ON `consultas`.`pacientes` TO 'medico'@'%';

GRANT SELECT, UPDATE (CitObservaciones, citEstado) ON `consultas`.`citas` TO 'medico'@'%';


# Privilegios para `paciente`@`%`

GRANT USAGE ON *.* TO 'paciente'@'%';

GRANT SELECT (dniMed, medNombres, medApellidos) ON `consultas`.`medicos` TO 'paciente'@'%';

GRANT SELECT ON `consultas`.`consultorios` TO 'paciente'@'%';

GRANT SELECT (citMedico, citHora, CitObservaciones, citPaciente, citEstado, citFecha, citConsultorio) ON `consultas`.`citas` TO 'paciente'@'%';