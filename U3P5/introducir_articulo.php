<?php

session_start();

?>

<!DOCTYPE html>
<html lang="es">
<head>
	<title>Introducir nuevo artículo</title>
	<meta charset="utf-8">
</head>
<body>
	<h2>Bienvenido/a <?php echo $_SESSION['usuario']; ?>, se ha identificado como <?php echo $_SESSION['rol'] ?></h2>
	<div class="backClose">
		<form action="" method="POST">
			<button type="submit" name="back">Volver al menú</button>
			<button type="submit" name="logout">Cerrar Sesión</button>
		</form>
	</div>
	<form action="" method="POST">
		<fieldset>
			<legend>Introducir nuevo artículo</legend>
			<p>Descripción: <input type="text" name="desc" required="required" maxlength="20" placeholder="Campo obligatorio"></p>
			<p>Precio: <input type="number" name="precio" required="required" placeholder="Campo obligatorio" step="0.01"></p>
			<p>Características: <br/><textarea name="caract" placeholder="Introducir las características del artículo" style="box-sizing: border-box; width: 350px; height: 200px; resize: none; overflow: auto;"></textarea></p>
			<p><input type="submit" name="insertar" value="Insertar artículo"></p>
		</fieldset>
	</form>

	<?php

	if (isset($_POST['insertar'])) {
		$descripcion=$_POST['desc'];
		$precio=$_POST['precio'];
		$caracteristicas=$_POST['caract'];

		if ($_SESSION['rol']=="administrador") {
			$conexion=mysqli_connect($_SESSION['servidor'], $_SESSION['usu1'], $_SESSION['pass1'], $_SESSION['basedatos']);
			if (mysqli_connect_errno()) {
	    		printf("Conexión fallida %s\n", mysqli_connect_error());
	    		exit();
			}

			$sql="INSERT INTO articulos (idarticulo,descripcion,precio,caracteristicas) VALUES (NULL,'$descripcion',$precio,'$caracteristicas');";
			if (mysqli_query($conexion, $sql)) {
				$mensajeregistro="Se ha registrado el artículo con éxito";
				echo $mensajeregistro;
			}
			else {
				echo " <br> Error: " . $sql . "<br>" . mysqli_error($conexion);
			}
		}

		mysqli_close($conexion);

	}

	if (isset($_POST['back'])) {

		header("Location:inicio.php");

	}

	if (isset($_POST['logout'])) {

		session_destroy();
			 
		header("Location:index.php");
	}

	?>
</body>
</html>