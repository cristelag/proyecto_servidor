<?php

session_start();

?>

<!DOCTYPE html>
<html lang="es">
<head>
	<title>Listado de artículos</title>
	<meta charset="utf-8">
</head>
<body>
	<h2>Bienvenido/a <?php echo $_SESSION['usuario']; ?>, se ha identificado como <?php echo $_SESSION['rol'] ?></h2>
	<div class="backClose">
		<form action="" method="POST">
			<button type="submit" name="back">Volver al menú</button>
			<button type="submit" name="logout">Cerrar Sesión</button>
		</form>
	</div>

	<?php

		$conexion=mysqli_connect($_SESSION['servidor'], $_SESSION['usu2'], $_SESSION['pass2'], $_SESSION['basedatos']);
			if (mysqli_connect_errno()) {
	    		printf("Conexión fallida %s\n", mysqli_connect_error());
	    		exit();
			}

	?>

	<table border="1" style="text-align: center;">
		<tr>
			<th>ID artículo</th>
			<th>Descripción</th>
			<th>Precio</th>
			<th>Características</th>
		</tr>

	<?php
	
	$sql="SELECT * FROM articulos;";

	$result = mysqli_query ($conexion, $sql);
	$filas=mysqli_num_rows($result);
	if ($filas>0) {
		while ($registro = mysqli_fetch_row($result)) {

	?>

		<tr>
			<td><?php echo $registro[0]; ?></td>
			<td><?php echo $registro[1]; ?></td>
			<td><?php echo $registro[2]; ?></td>
			<td><?php echo $registro[3]; ?></td>
		</tr>

	<?php

			}
		}
		else {
			echo "<tr><td colspan='4'>No hay ningún artículo</td></tr>";
		}

	if (isset($_POST['back'])) {

		header("Location:inicio.php");

	}

	if (isset($_POST['logout'])) {

		session_destroy();
			 
		header("Location:index.php");
	}

	mysqli_close($conexion);

	?>

	</table>	
</body>
</html>