<!DOCTYPE html>
<html lang="es">
<head>
	<title>Registro de usuario</title>
	<meta charset="utf-8">
</head>
<body>
	<h2>Va a registrarse en el sistema como Consultor</h2>
	<div class="backClose">
		<form action="" method="POST">
			<button type="submit" name="inicio">Volver al inicio</button>
		</form>
	</div>
	<form action="" method="POST" name="regForm" onsubmit="return validar()">
		<p>Usuario: <input type="text" name="usu" required="required" maxlength="20"></p>
		<p>Contraseña: <input type="password" name="pass" required="required" maxlength="50"></p>
		<p>Repita la contraseña: <input type="password" name="rpass" id="rpasswd" required="required" maxlength="50" onblur="valpass()"><span id="avisopass"></span></p>
		<p><input type="submit" name="registrar" value="Registrarse"></p>
	</form>

	<?php

	if (isset($_POST['registrar'])) {
		$contra=$_POST['rpass'];
		$contraHash=hash_hmac("sha512", $contra, "primeraweb", FALSE);
		$user=$_POST['usu'];

		$conexion=mysqli_connect("localhost", "registroventas", "registroventas", "ventas");
			if (mysqli_connect_errno()) {
	    		printf("Conexión fallida %s\n", mysqli_connect_error());
	    		exit();
			}

		$sql="INSERT INTO usuarios (idusuario,usuario,password,rol) VALUES (NULL,'$user','$contraHash','consultor');";
		if (mysqli_query($conexion, $sql)) {
			$mensajeregistro="Se ha registrado el usuario con éxito";
			echo $mensajeregistro;
		}
		else {
			echo " <br> Error: " . $sql . "<br>" . mysqli_error($conexion);
		}
		mysqli_close($conexion);
	}

	if (isset($_POST['inicio'])) {

		header("Location:index.php");
	}

	?>

	<script>
		function validar() {
			if (valpass()) {
				return true;
			}
			else {
				alert ("Datos erróneos, indtroducir de nuevo");
				return false;
			}
		}

		function valpass() {
			var contra=document.regForm.pass.value;
			var rcontra=document.regForm.rpass.value;

			if (contra===rcontra) {
				document.getElementById('rpasswd').style.border="3px solid green";
				document.getElementById('avisopass').innerHTML=" &check; Contraseña correcta";
				return true;
			}
			else {
				document.getElementById('rpasswd').style.border="3px solid red";
				document.getElementById('avisopass').innerHTML=" &cross; Contraseña incorrecta";
				return false;
			}
		}
	</script>
</body>
</html>