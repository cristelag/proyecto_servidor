<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use App\Movie;
use App\User;

class CatalogController extends Controller
{

    public function getShow($id)
    {
        $pelicula = Movie::find($id);
        return view('catalog.show', array('pelicula' => $pelicula));
    }
    public function getIndex()
    {
        $peliculas = Movie::all();
        return view('catalog.index', array('arrayPeliculas' => $peliculas));
    }
    public function getEdit()
    {
        return view('catalog.edit');
    }
    public function getCreate()
    {
        return view('catalog.create');
    }

    public function postCreate(Request $request)
    {
        $pelicula = new Movie;
        $pelicula->title = $request->input('title');
        $pelicula->year = $request->input('year');
        $pelicula->director = $request->input('director');
        $pelicula->poster = $request->input('poster');
        $pelicula->synopsis = $request->input('synopsis');
        $pelicula->save();
        return redirect('catalog');
    }
    public function putEdit(Request $request, $id)
    {
        $pelicula = new Movie;
        $pelicula->title = $request->input('title');
        $pelicula->year = $request->input('year');
        $pelicula->director = $request->input('director');
        $pelicula->poster = $request->input('poster');
        $pelicula->synopsis = $request->input('synopsis');
        $pelicula->save();
        return redirect('catalog');
    }
    // public function postEdit(Request $request)
    // {
    //     $pelicula = new Movie;
    //     $pelicula->title = $request->input('title');
    //     $pelicula->year = $request->input('year');
    //     $pelicula->director = $request->input('director');
    //     $pelicula->poster = $request->input('poster');
    //     $pelicula->synopsis = $request->input('synopsis');
    //     $pelicula->save();
    //     return redirect('catalog');
    // }
}
