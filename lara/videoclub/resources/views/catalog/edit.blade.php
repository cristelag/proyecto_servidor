@extends('layout.master')
@section('content')
<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="UTF-8">

    <title>Edit</title>
</head>

<body>
    <form action="{{ route('catalog.edit',$pelicula->id) }}" method="POST">

        <input type="hidden" name="_method" value="PUT"/> 
        <input type="hidden" name="_token" value="{{ csrf_token() }}"/> 

        <fieldset>
            <h1>Edita la película</h1>
            Título:<input type="text" value="<?php echo $pelicula['title'];?>". name="title" id="title" required></input><br>
            Año:<input type="number"  value="<?php echo $pelicula['year'];?>". name="year" id="year" required></input><br>
            Director:<input type="text"  value="<?php echo $pelicula['director']; ?>". name="director" id="director" required></input><br>
            Imagen: <input type="url"  value="<?php echo $pelicula['poster'];?>". name="poster" id="poster" required></input><br>
            Sipnosis:<textarea rows="4" cols="50" name="synopsis" id="synopsis" required><?php echo $pelicula['synopsis'];?></textarea><br>
            <button type="submit" name="env" id="env">Modificar Película</button>
        </fieldset>
    </form>

</body>

</html>

@stop