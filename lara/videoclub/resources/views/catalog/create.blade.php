@extends('layout.master')
@section('content')
<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="UTF-8">

    <title>Create</title>
</head>

<body>
    <form action="{{ action('CatalogController@postCreate') }}" method="POST">
        @csrf
        <!-- <input type="hidden" name="_token" value="{{ csrf_token() }}"> -->
        <fieldset>
            <h1>Inserta nueva película</h1>
            Título:<input type="text" name="title" id="title" required></input><br>
            Año:<input type="number" name="year" id="year" required></input><br>
            Director:<input type="text" name="director" id="director" required></input><br>
            Imagen: <input type="url" name="poster" id="poster" required></input><br>
            Sipnosis:<input type="text" name="synopsis" id="synopsis" required></input><br>
            <button type="submit" name="env" id="env">Añadir Pelicula</button>
        </fieldset>
    </form>

</body>

</html>
@stop