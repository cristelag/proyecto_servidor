@extends('layout.master')
@section('content')
<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="UTF-8">
    <title>Listado de películas</title>
    <style>
        ul{
            list-style-type: none;
        }
        img{
            width:auto;
            height:200px;
        }
    </style>
</head>

<body>
    <?php

    foreach ($arrayPeliculas as $pelicula) {
        echo "<ul>";
        echo "<li><img src=" . $pelicula['poster'] . "></li>";
        echo  "<li>".$pelicula['title'] . "</li>";
        echo "</ul>";
    }

    ?>
</body>

</html>
@stop