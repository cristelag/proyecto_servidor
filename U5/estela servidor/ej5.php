<!DOCTYPE html>
<html>
<head>
<style>
	.main{
		margin-top: 15%;
	}
	span {
  		height: 25px;
  		width: 25px;
  		background-color: red;
  		border-radius: 50%;
  		display: block;
  		position: absolute;
	}
	button{
		width: 70px;
		height: 50px;
		font-size: 80%;
		font-weight: bold;
		display: block;
	}
	.botones{
		margin-top: -100px;
		width: 74px;
	}
</style>
</head>
<body>
<?php
session_start();
if (isset($_SESSION['estelas'])==false){
	$_SESSION['estelas'] = [];
}
if (isset($_SESSION['posicion'])==false){
	$posicion = 50;
	$_SESSION['posicion'] = 50;
} else {
	$posicion = $_SESSION['posicion'];
}

if (isset($_SESSION['posicionY'])==false){
	$posicionY = 0;
	$_SESSION['posicionY'] = 0;
} else {
	$posicionY = $_SESSION['posicionY'];
}
?>
<div class="main">
	<div class="botones">
		<form action = "ej5b.php" method="post">
			<button type="submit" name="arriba">Arriba</button>
	 		<button type="submit" name="izq">Izquierda</button>
	 		<button type="submit" name="centro">Volver al centro</button>
	 		<button type="submit" name="dcha">Derecha</button>
	 		<button type="submit" name="abajo">Abajo</button>
	 	</form>
	</div>
	<div id="punto">
		<span style="margin-left: <?php echo $posicion.'%';?>; margin-top: <?php echo $posicionY.'%';?>"></span>
		<?php 
			for ($i=0;$i<count($_SESSION['estelas']);$i++){
				?><span style="z-index: -1; background-color: #ff8080; margin-left: <?php echo $_SESSION['estelas'][$i][0].'%';?>; margin-top: <?php echo  $_SESSION['estelas'][$i][1].'%';?>"></span><?php
			}
		?>
	</div>
</div>
</body>
</html>