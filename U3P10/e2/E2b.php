<?php
//se accede a la sesion
session_start();

//si la posicion no esta guardada en la sesion, la pone a 0
if (!isset($_SESSION["posicion"])){
    $_SESSION["posicion"]=0;
}
function recoge($var){
    $tmp=(isset($_REQUEST[$var]))? trim(htmlspecialchars($_REQUEST[$var],ENT_QUOTES, "UTF-8")):"";
    return $tmp;
}

$accion =recoge("accion");
$accionOk=true;

if($accion!="centro" && $accion!="izquierda"&& $accion!="derecha"){
    header("Location:E2.php");
    exit;
}else{
    $accionOk=true;
}

if($accionOk){
    if($accion=="centro"){
        $_SESSION["posicion"]=150;
    }elseif ($accion=="izquierda"){
        $_SESSION["posicion"]-=20;
    }elseif($accion=="derecha"){
        $_SESSION["posicion"]+=20;
    }
    if($_SESSION['posicion']>300){
        $_SESSION['posicion']=0;
    }
    if($_SESSION['posicion']<0) {
        $_SESSION['posicion']=300;
    }
header("Location:E2.php");
exit;
}
?>
