<?php

session_start();

if(!isset($_SESSION["a"])||!isset($_SESSION["b"])){
    $_SESSION["a"]=$_SESSION["b"]=0;
}
?>
<!DOCTYPE html>
<html lang="es">
    <head>
        <title></title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        
    </head>
    <body>

    <form action="confirmar.php" method="POST">
    <table>
    <tr>
    <td style="vertical-align:top;"><button type="submit" name="accion" value="a" style="font-size: 60px; line-height:50px; color:black; padding:5px;">🌸</button>
    </td>
    <td>
    <?php

    print "<svg version=\"1.1\" xmlns=\"http://www.w3.org/2000/svg\"\n";
    print " width=\"$_SESSION[a]\"height=\"50\">\n";
    print "<line x1=\"0\" y1=\"25\" x2=\"$_SESSION[a]\" y2=\"25\" stroke=\"rgb(177,142,166)\" stroke-width=\"50\"/>\n";
    print " </svg>\n";
    ?>
</td>
    </tr>
    <tr>
    <td style="vertical-align:top;"><button type="submit" name="accion" value="b" style="font-size: 60px; line-height:50px; color:black; padding:5px">🌷</button>
    </td>
    <td>
    <?php
    print "<svg version=\"1.1\" xmlns=\"http://www.w3.org/2000/svg\" \n"; 
    print "width=\"$_SESSION[b]\"height=\"50\">\n";
    print "<line x1=\"0\" y1=\"25\" x2=\"$_SESSION[b]\" y2=\"25\" stroke=\"rgb(178,228,213)\" stroke-width=\"50\"/>\n";
    print " </svg>\n";
    ?>
    </td>
    </tr>
    </table>
<button type="submit" name="accion" value="cero">Reset!</button>
    </form>    
    </body>
</html>