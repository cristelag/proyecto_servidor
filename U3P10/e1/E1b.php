<?php

session_start();

if(!isset($_SESSION["numero"])){
	$_SESSION["numero"]=0;
}
function recoge($var){
	$tmp=(isset($_REQUEST[$var]))
	? trim(htmlspecialchars($_REQUEST[$var], ENT_QUOTES, "UTF-8")): "";
	return $tmp;
}

$accion=recoge("accion");
$accionOk=false;

if($accion !="cero" && $accion != "subir" && $accion!= "bajar"){
	header("Location:E1.php");
	exit;
}else{
	$accionOk=true;
}

if($accionOk){
	if($accion=="cero"){
		$_SESSION["numero"]=0;
	}elseif ($accion=="subir"){
		$_SESSION["numero"]++;
	}elseif ($accion=="bajar"){
		$_SESSION["numero"]--;
	}
header("Location:E1.php");
exit;
}
?>