<?php

session_start();

//si las posiciones no estan guardadas en sesion. se ponen a 0
if(!isset($_SESSION["x"])|| !isset($_SESSION["y"])){
    $_SESSION["x"]=$_SESSION["y"]=0;
}
?>
<!DOCTYPE html>
<html lang="es">
    <head>
        <title></title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
       <style>
       #cen{
           width:auto;
           height:50px;
       }
       </style>
    </head>
    <body>
<form action="confirmar.php" method="POST">
    
    <button type="submit" name="accion" value="izquierda" ><img src="izqda.svg" width="50px" height="auto"></button>
    <button type="submit" name="accion" value="arriba" ><img src="arriba.svg" width="50px" height="auto"></button>
    <button id="cen" type="submit" name="accion" value="centro" >\( ͡❛ ͜ʖ ͡❛)/</button>
    <button type="submit" name="accion" value="derecha" ><img src="der.png" width="50px" height="auto"></button>
    <button type="submit" name="accion" value="abajo" ><img src="abajo.png" width="50px" height="auto"></button>
    <div>
    <td>
    <svg version="1.1" xmlns="https://www.w3.org/2000/svg" width="400" height="400" viewbox="-200 -200 400 400" style="border: black 2px solid">
    <?php
        print "<circle cx=\"$_SESSION[x]\" cy=\"$_SESSION[y]\" r=\"8\" fill=\"red\" />\n";
        ?>
        </svg>
    </td>
  </div>
</form>

    </body>
</html>