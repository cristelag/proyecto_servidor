<?php
//se accede a la sesion
session_start();

//si la posicion no esta guardada en la sesion, la pone a 0
if (!isset($_SESSION["x"]) || !isset($_SESSION["y"])){
    $_SESSION["x"]=$_SESSION["y"]=0;
}
function recoge($var){
    $tmp=(isset($_REQUEST[$var]))? trim(htmlspecialchars($_REQUEST[$var],ENT_QUOTES, "UTF-8")):"";
    return $tmp;
}

$accion =recoge("accion");
$accionOk=true;

if($accion!="centro" && $accion!="izquierda"&& $accion!="derecha" && $accion!="arriba" && $accion!="abajo"){
    header("Location:E3.php");
    exit;
}else{
    $accionOk=true;
}

if($accionOk){
    if($accion=="centro"){
        $_SESSION["x"]=$_SESSION["y"]=0;
    }elseif ($accion=="izquierda"){
        $_SESSION["x"]-=20;
    }elseif($accion=="derecha"){
        $_SESSION["x"]+=20;
    }elseif($accion=="arriba"){
        $_SESSION["y"]-=20;
    }elseif($accion=="abajo"){
        $_SESSION["y"]+=20;
    }

    if($_SESSION['x']>200){
        $_SESSION['x']=-200;
    }
    if($_SESSION['x']<(-200)) {
        $_SESSION['x']=200;
    }
    if($_SESSION['y']>200){
        $_SESSION['y']=-200;
    }
    if($_SESSION['y']<(-200)) {
        $_SESSION['y']=200;
    
    }
header("Location:E3.php");
exit;
}
?>
