<!DOCTYPE HTML>
<html lang="es">
<head>
	<title>Plantilla básica</title>
	<meta charset='UTF-8'/>
	<style>
    .error {color: #FF0000;}
    </style>
</head>
<body> 
<?php
$nameErr = $emailErr = $genderErr = $urlErr = "";
$nombre = $email = $gender = $comentarios = $url = "";

if ($_SERVER["REQUEST_METHOD"] == "POST") {
  if (empty($_POST["nombre"])) {
    $nameErr = "Name is required";
  } else {
    $nombre = test_input($_POST["nombre"]);
    
    if (!preg_match("/^[a-zA-Z ]*$/",$nombre)) {
      $nameErr = "Only letters and white space allowed";
    }
  }

  if (empty($_POST["email"])) {
    $emailErr = "Email is required";
  } else {
    $email = test_input($_POST["email"]);
 
    if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
      $emailErr = "Invalid email format";
    }
  }

  if (empty($_POST["URL"])) {
    $url = "";
  } else {
    $url = test_input($_POST["URL"]);
 
    if (!preg_match("/\b(?:(?:https?|ftp):\/\/|www\.)[-a-z0-9+&@#\/%?=~_|!:,.;]*[-a-z0-9+&@#\/%=~_|]/i",$url)) {
      $urlErr = "Invalid URL";
    }
  }

  if (empty($_POST["comentarios"])) {
    $comentarios = "";
  } else {
    $comentarios= test_input($_POST["comentarios"]);
  }

  if (empty($_POST["gender"])) {
    $genderErr = "Gender is required";
  } else {
    $gender = test_input($_POST["gender"]);
  }
}
function test_input($data) {
    $data = trim($data);
    $data = stripslashes($data);
    $data = htmlspecialchars($data);
    return $data;
  }
  
?>

<h2>PHP Form Validation Example</h2>
<p><span class="error">* required field</span></p>
<form action="" method="POST" id="formu">
Name: <input type="text" name="nombre" value="<?php echo $nombre;?>"><span class="error">*<?php echo $nameErr;?></span>
<br><br>
E-mail: <input type="text" name="email" value="<?php echo $email;?>"><span class="error">*<?php echo $emailErr;?></span>
<br><br>
Website: <input type="text" name="URL" value="<?php echo $url;?>">
<br><br>
Comment:<textarea name="comentarios" rows="6" cols="40"><?php echo $comentarios;?></textarea>
<br><br>
<input type="radio" name="gender" <?php if (isset($gender) && $gender=="female") echo "checked";?>
value="female">Female
<input type="radio" name="gender" <?php if (isset($gender) && $gender=="male") echo "checked";?>
value="male">Male
<span class="error">* <?php echo $genderErr;?></span>
<br><br>
<input type="submit" name="enviar" value="Enviar">
</form>




</body>
</html>