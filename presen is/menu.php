<?php
session_start();

?>
<!DOCTYPE html>
<html lang="es">

<head>
	<title>Inicio</title>
	<meta charset="utf-8">
</head>

<body>
	<?php
	if ($_SESSION['rol'] == "director") {
		require('conexion1.php');
	?>

		<h3>Bienvenido/a <?php echo $_SESSION['usu']; ?>, se ha conectado como <?php echo $_SESSION['rol'] ?></h3>
		<form action="#" method="POST">
			<div><button type="submit" name="ins">Introducir alumnoso</button></div>
			<div><button type="submit" name="ver">Ver Notas</button></div>
			<div><button type="submit" name="logout">Cerrar Sesion</button></div>
		</form>

		<?php
		if (isset($_POST['ins'])) {
			header("location:introducir.php");
		}
		if (isset($_POST['ver'])) {
			header("location:vernotas.php");
		}
		if (isset($_POST['logout'])) {
			session_destroy();
			header("location:login.php");
		}
	}

	if ($_SESSION['rol'] == "alumno") {
		require('conexion2.php');

		?>

		<h3>Bienvenido/a <?php echo $_SESSION['usu']; ?>, se ha conectado como <?php echo $_SESSION['rol'] ?></h3>
		<form action="#" method="POST">
			<div><button type="submit" name="ver">Ver Notas</button></div>
			<div><button type="submit" name="logout">Cerrar Sesión</button></div>
		</form>

	<?php
		if (isset($_POST['ver'])) {
			header("location:vernotas.php");
		}
		if (isset($_POST['logout'])) {
			session_destroy();
			header("location:login.php");
		}
	}
	mysqli_close($conexion);
	?>
</body>

</html>