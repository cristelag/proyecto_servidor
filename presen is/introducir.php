<?php

session_start();

?>

<!DOCTYPE html>
<html lang="es">

<head>
	<title>Introducir nuevo artículo</title>
	<meta charset="utf-8">
</head>

<body>
	<h2>Bienvenido/a <?php echo $_SESSION['usu']; ?>, se ha conectado como <?php echo $_SESSION['rol'] ?></h2>


	<form action="#" method="POST">
		<legend>Introducir nuevo Alumno</legend>
		<p>Nombre: <input type="text" name="nombre" required="required" maxlength="25"></p>
		<p>Apellidos: <input type="text" name="apell" required="required" placeholder="Campo obligatorio" step="0.01"></p>
		<p>Localidad: <input type="text" name="local" placeholder="Introducir la localidad" required="required"></p>
		<p>Contraseña:<input type="password" name="pass" placeholder="Introducir la Cotraseña" required="required"></p>
		<p>Repetir contraseña:<input type="password" name="repass" placeholder="Repetir la Cotraseña" required="required"></p>
		<p><input type="submit" name="insertar" value="Insertar alumno"></p>
	</form>
	<form action="" method="POST">
		<button type="submit" name="atrs">Volver al menú</button>
		<button type="submit" name="logout">Cerrar Sesión</button>
	</form>
	<?php

	if (isset($_POST['insertar'])) {
		$nombre = $_POST['nombre'];
		$apell = $_POST['apell'];
		$localidad = $_POST['local'];
		$pass = $_POST['pass'];
		$repass = $_POST['repass'];


		if ($_POST['pass'] != $_POST['repass']) {
			echo "<p>Las contraseñas no coinciden</p>";
		} else {

			if ($_SESSION['rol'] == "director") {
				require('conexion1.php');

				$sqlcomp = "SELECT nombre,apellidos,localidad from usuarios where nombre='$nombre' and apellidos='$apell' and localidad='$localidad'";
				$res = mysqli_query($conexion, $sqlcomp);
				$valor = mysqli_num_rows($res);
				
				if ($valor != 0) {
					echo "<p>Usuario ya existe</p>";
				} else {
					$sql = "INSERT INTO usuarios (id,nombre,apellidos,login,password,rol,localidad) VALUES (NULL,'$nombre','$apell','$nombre','$pass','alumno','$localidad');";
					if (mysqli_query($conexion, $sql)) {
						echo "<p>Se ha registrado el artículo correctamente</p>";
					} else {
						echo " <br> Error: " . $sql . "<br>" . mysqli_error($conexion);
					}
				}
			}
			mysqli_close($conexion);
		}
	}
	if (isset($_POST['atrs'])) {
		header("location:menu.php");
	}

	if (isset($_POST['logout'])) {
		session_destroy();
		header("location:login.php");
	}

	?>
</body>

</html>