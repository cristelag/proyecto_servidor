<?php
session_start();
?>
<!DOCTYPE html>
<html lang="es">

<head>
	<title>Listado de Notas</title>
	<meta charset="utf-8">
	<style>
		table {
			margin: 20px;
			float: left;

		}

		.container {
			display: inline;

		}
	</style>
</head>

<body>
	<h2>Bienvenido/a <?php echo $_SESSION['usu']; ?>, se ha conectado como <?php echo $_SESSION['rol'] ?></h2>
	<div class="uno">
		<form action="#" method="POST">
			<button type="submit" name="atras">Volver al menú</button>
			<button type="submit" name="salir">Cerrar Sesión</button>
		</form>
	</div>



	<div class="container">
		<?php
		if ($_SESSION['rol'] == 'director') {
			$notasalu[] =  array();
		?>


			<form action="#" method="POST">
				Asignatura: <input type="text" name="asig" required="required">
				<button type="submit" name="mostrar">Mostrar notas</button>
			</form>


			<?php
			require('conexion1.php');
			if (isset($_POST['mostrar'])) {
				$asignatura = $_POST['asig'];

				$sqlasig = "SELECT asignatura FROM notas where asignatura='$asignatura';";
				$result = mysqli_query($conexion, $sqlasig);
				$v = mysqli_num_rows($result);
				if ($v != 0) {

					$sql = "SELECT alumno,fecha,nota FROM notas where asignatura='$asignatura';";
					$result2 = mysqli_query($conexion, $sql);
					$valor = mysqli_num_rows($result2);

					if ($valor > 0) {

			?>
						<h4>Resultados de la asignatura <?php echo $asignatura; ?></h4>
						<table>
							<tr class="arti">
								<th>ID alumno</th>
								<th>Fecha</th>
								<th>Nota</th>
							</tr>
							<?php
							while ($registro = mysqli_fetch_row($result2)) {
							?>
								<tr class="arti">
									<td><?php echo $registro[0]; ?></td>
									<td><?php echo $registro[1]; ?></td>
									<td><?php echo $registro[2]; ?></td>
								</tr>
							<?php
								array_push($notasalu, $registro[2]);
							}

							$totalalu = count($notasalu) - 1;
							$suma = array_sum($notasalu);
							$media = $suma / $totalalu;
							$notamin = min($notasalu);



							?>

						</table>

				<?php
						echo " <p>Nota media: " . $media . "<br></p>";
						echo "<p>Nota mas baja: " . $notamin . "<br></p>";
						echo "<p>Nota mas alta: " . max($notasalu) . "<br></p>";
						echo "<p>Total de alumnos es: " . $totalalu . "<br> </p>";
					} else {
						echo "<p>No se encuentran notas de esta asignatura</p>";
					}
				} else {
					echo "<p> No se encuentra notas de esta asignatura</p>";
				}
			}
		} else	if ($_SESSION['rol'] == 'alumno') {
			require('conexion2.php');
			$idalu = $_SESSION['id'];
			$notass[] =  array();
			$suspensas= 0;


			$sqlalu = "SELECT asignatura,fecha,nota FROM notas where alumno = '$idalu';";
			$result3 = mysqli_query($conexion, $sqlalu);
			$valor2 = mysqli_num_rows($result3);
			if ($valor2 > 0) {
				?>
				<h4>Sus resultados academicos son: </h4>
				<table>
					<tr class="arti">
						<th>Asignatura</th>

						<th>Fecha</th>

						<th>Nota</th>

					</tr>

					<?php
					while ($registro = mysqli_fetch_row($result3)) {
					?>
						<tr class="arti">
							<td><?php echo $registro[0]; ?></td>
							<td><?php echo $registro[1]; ?></td>
							<td><?php echo $registro[2]; ?></td>
						</tr>
					<?php
						array_push($notass, $registro[2]);
					}
					?>
				</table>
		<?php
				for ($i = 0; $i < count($notass); $i++) {
					if(	$notass[$i] <5){
						$suspensas++;
					}
					
				}
				if($suspensas>0){
					echo "Has suspendido ".$suspensas." materias" ;
				}
			} else {
				echo "<p>No se encuentran notas</p>";
			}
		}
		if (isset($_POST['atras'])) {
			header("location:menu.php");
		}

		if (isset($_POST['salir'])) {
			session_destroy();
			header("location:login.php");
		}
		mysqli_close($conexion);

		?>


	</div>
</body>

</html>