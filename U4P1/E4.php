<?php
class Persona
{
    public $nombre;
    public $edad;
    public function __construct($nombre, $edad)
    {
        $this->nombre = $nombre;
        $this->edad = $edad;
        echo 'La clase "', __CLASS__, '" se ha iniciado<br />';
    }

    public function getNombre()
    {
        return $this->nombre;
    }

    public function getEdad()
    {
        return $this->edad;
    }
    public function __destruct()
    {
        echo 'La clase "', __CLASS__, '" ha sido destruida.<br />';
    }
}

class Empleado extends Persona
{
    public $sueldo;
    public function __construct($nombre, $edad, $sueldo)
    {
        $this->edad = $edad;
        $this->nombre = $nombre;
        $this->sueldo = $sueldo;
        echo 'La clase "', __CLASS__, '" se ha iniciado<br />';
    }
    public function getSueldo()
    {
        return $this->sueldo;
    }
}
$persona = new Persona('Cristel', 19);
echo "Datos personales de la persona:";
echo "<br>Nombre: " . $persona->getNombre() . "<br>";
echo "Edad: " . $persona->getEdad() . "<br>";

var_dump($persona);
echo "<br>";

$emp = new Empleado('Manolo', 50, 2000);
echo "Datos personales del empleado:";
echo "<br>Nombre: " . $emp->getNombre() . "<br>";
echo "Edad: " . $emp->getEdad() . "<br>";
echo "Sueldo: " . $emp->getSueldo() . "<br>";

var_dump($emp);
echo "<br>";
