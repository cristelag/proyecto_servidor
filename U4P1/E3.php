
<?php
class Empleado
{
    public $nombre;
    public $sueldo;

    public function __construct($nombre, $sueldo)
    {
        $this->nombre = $nombre;
        $this->sueldo = $sueldo;
        echo 'La clase "', __CLASS__, '" se ha iniciado<br />';
    }
    public function __destruct()
    {
        echo 'La clase "', __CLASS__, '" ha sido destruida.<br />';
    }

    public function getNombre()
    {
        return $this->nombre;
    }

    public function getSueldo()
    {
        return $this->sueldo;
    }
}

function impuestos($trabajador)
{
    if ($trabajador->getSueldo() > 3000) {
        return $trabajador->getNombre() . " Debe pagar impuestos";
    } else {
        return $trabajador->getNombre() . " No debe pagar impuestos";
    }
}

$emp1 = new Empleado('Bea', 2500);
$respuesta = impuestos($emp1);
echo "<br>" . $respuesta . "<br>";
var_dump($emp1);


$emp2 = new Empleado('Cristel', 2400);
$respuesta = impuestos($emp2);
echo "<br>" . $respuesta . "<br>";
var_dump($emp2);
