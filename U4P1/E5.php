<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>

<body>
    <?php
    class Libros
    {
        public $titulo;
        public $autor;
        public $anio;
        public $hojas;
        public $editorial;
        public function __construct($titulo, $autor, $anio, $hojas, $editorial)
        {
            $this->titulo = $titulo;
            $this->autor = $autor;
            $this->anio = $anio;
            $this->hojas = $hojas;
            $this->editorial = $editorial;
            echo 'La clase "', __CLASS__, '" se ha iniciado<br />';
        }
        public function getTitulo()
        {
            return $this->titulo;
        }
        public function getAutor()
        {
            return $this->autor;
        }
        public function getAnio()
        {
            return $this->anio;
        }
        public function getHojas()
        {
            return $this->hojas;
        }
        public function getEditorial()
        {
            return $this->editorial;
        }
    }
    $lib = new Libros('Neimhaim', 'Aranzazu Serrano', 2016, 563, 'Delibes');
    echo "<h1>" . $lib->getTitulo() . "</h1>";
    echo "<br>Título: " . $lib->getTitulo() . "<br>";
    echo "Autor: " . $lib->getAutor() . "<br>";
    echo "Año: " . $lib->getAnio() . "<br>";
    echo "Hojas: " . $lib->getHojas() . "<br>";
    echo "Editorial: " . $lib->getEditorial() . "<br>";




    var_dump($lib);
    echo "<br>";
    ?>
</body>

</html>