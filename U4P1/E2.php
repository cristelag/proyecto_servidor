
<?php
class empleado
{
    public $nombre;
    public $sueldo;

    public function __construct($nombre, $sueldo)
    {
        $this->nombre = $nombre;
        $this->sueldo = $sueldo;
    }

    public function getNombre()
    {
        return $this->nombre;
    }

    public function getSueldo()
    {
        return $this->sueldo;
    }
}

function impuestos($trabajador)
{
    if ($trabajador->getSueldo() > 3000) {
        return $trabajador->getNombre() . " Debe pagar impuestos";
    } else {
        return $trabajador->getNombre() . " No debe pagar impuestos";
    }
}

$emp1 = new empleado('Bea', 2500);
$respuesta = impuestos($emp1);
echo "<br>" . $respuesta . "<br>";
var_dump($emp1);


$emp2 = new empleado('Cristel', 2400);
$respuesta = impuestos($emp2);
echo "<br>" . $respuesta . "<br>";
var_dump($emp2);
